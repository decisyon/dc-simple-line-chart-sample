(function() {
  'use strict';
  
  /**
   * Monica Modena
   * The service DcyHTTPRequestService manages the HTTP request.
   * 
   * @params requestParams {object} parameters in JSON format
   * 
   * Example of requestParams:
   * - url {string} url path
   * - params.revent {string} event to call
   * - params.rasterCtx {string} context (serialized in JSON)
   * 
   * TODO: mockData management
   */
  function DcyHTTPRequestService($q, $http, $log, dcyEnvProvider, dcyUtilsFactory, dcyRequestErrorHandler, $httpParamSerializerJQLike, dcyConstants) {

	  function getResponse(requestParams) {
		  
		  if (!dcyUtilsFactory.isValidObject(requestParams)) {
			  $log.error('#DcyHTTPRequestService <DCY_ERROR> -> requestParams is not a valid object');
			  return false;
		  }
		  
		  var deferred = $q.defer(),
			  domainUrl = dcyEnvProvider.getItem('domainUrl', null, 'applicationInfo'),
			  contextPath = dcyEnvProvider.getItem('contextPath'),
			  serviceName = (angular.isDefined(requestParams.url)) ? requestParams.url : domainUrl + contextPath + requestParams.subPath,
			  reqParams = {
					  headers : {
							acceptCtx : 'V1'
					  },
					  method: angular.isDefined(requestParams.httpRequestType) ? requestParams.httpRequestType : 'GET',
					  timeout: 7200000,
					  url: serviceName,
					  cache: false,
					  responseTypeText : requestParams.responseTypeText ? requestParams.responseTypeText : false
			  };

		  //Nel caso di chiamata in POST bisogna utilizzare la propriet� 'data'
		  if(angular.equals(reqParams.method, 'POST')){
			  reqParams.data = $httpParamSerializerJQLike(requestParams.params);
			  reqParams.headers['Content-Type'] = dcyConstants.HTTP_POST_CONTENT_TYPE;
		  }
		  //Nel caso di chiamata in GET bisogna utilizzare la propriet� 'params'
		  else{
			  reqParams.params = $httpParamSerializerJQLike(requestParams.params);
		  }
		  
		  /**
		   * Success callback
		   * @param response
		   * @returns
		   */
		  function successCallback(response){
			  return deferred.resolve(response);  
		  }
		  
		  /**
		   * Error callback
		   * @param rejection
		   * @returns
		   */
		  function errorCallback(rejection){
			  $log.error('#DcyHTTPRequestService <DCY_ERROR> ->  ', rejection.status, '-> response ', rejection);
			  dcyRequestErrorHandler.responseError(rejection);
			  return deferred.reject({
				  response: rejection,
				  status: rejection.status,
				  headers: rejection.headers,
				  config: rejection.config
			  });
		  }
		  
		  function doAngularHttpRequest(){
			  $http(reqParams).then(
					function(response) {			  				
						return successCallback(response);  
					}, 
					function(rejection) {
						return errorCallback(rejection);
					});
		  }
		  
		  function doXmlHttp(){
		      var xmlHttp = new XMLHttpRequest();
		      
		      var isRequestSuccessful = function() {
			      var success = (xmlHttp.status == 0 || 
			          (xmlHttp.status >= 200 && xmlHttp.status < 300) || 
			          xmlHttp.status == 304 || xmlHttp.status == 1223);//IE: sometimes 1223 instead of 204
			      
			      return success;
			  },
		      
			  getResponseObject = function(){
				  return {
    				  statusText : xmlHttp.statusText,
    				  header	: {
    					  contentType 	: xmlHttp.getResponseHeader('Content-Type'),
    					  contentLength : xmlHttp.getResponseHeader('Content-Length'),
    					  lastModified	: xmlHttp.getResponseHeader('Last-Modified')
    				  },
    				  config 	: reqParams,
    				  status 	: xmlHttp.status,
					  data 		: xmlHttp.responseText
    			  };
			  };
			  
		      xmlHttp.onreadystatechange = function() { 
		    	  if(xmlHttp.readyState == 4){
		    		  if (isRequestSuccessful()) { 
		    			  successCallback(getResponseObject());
		    		  } else { 
		    			  errorCallback(getResponseObject());
		    		  } 
		    	  }
		      };
		      
		      xmlHttp.open(reqParams.method, reqParams.url, true);
		      // Timeout setting must follow the open to prevent error on IE-Browser
		      xmlHttp.timeout = reqParams.timeout; 
		      xmlHttp.ontimeout = function () {};
		      
		      if(!reqParams.cache){
		    	  xmlHttp.setRequestHeader("Cache-Control", "no-cache");
		      }
		      
		      if(angular.equals(reqParams.method, 'POST')){
		    	  xmlHttp.setRequestHeader("Content-type", dcyConstants.HTTP_POST_CONTENT_TYPE);
		    	  xmlHttp.send(reqParams.data);
		      }else{
		    	  xmlHttp.send(reqParams.params);
		      }
		  }
		  
		  
		  function runHttpRequest() {
			  if(reqParams.responseTypeText){
				  doXmlHttp();
			  }
			  else{
				  doAngularHttpRequest();
			  }
		  }
		  
		  runHttpRequest();
		
		  return deferred.promise;
	  }

	  return getResponse;
  }
  
  DcyHTTPRequestService.$inject = ['$q', '$http', '$log', 'dcyEnvProvider', 'dcyUtilsFactory', 'dcyRequestErrorHandler', '$httpParamSerializerJQLike', 'dcyConstants'];

  angular.module('dcyApp.services').service('dcyHTTPRequestService', DcyHTTPRequestService);

}());
