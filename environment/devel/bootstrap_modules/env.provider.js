(function() {
  'use strict';
  /**
   * @ngdoc service
   * @name dcyApp.dcyEnvProvider
   *
   * @description
   * Enviroment management
   *
   *
   *
   * @example
   * ```js
   * function serviceFn(dcyEnvProvider){
   *    dcyEnvProvider.callFn(params);
   * }
   * myFn.$inject = ['dcyEnvProvider'];
   *
   * angular.module('dcyApp.factories')
   *     .factory('service', serviceFn);
   *
   * ```
   */
  function EnvProvider() {


    /**
    * @ngdoc method
    * @name dcyApp.dcyEnvProvider.MainEnv
    * @methodOf dcyApp.dcyEnvProvider

    * @requires $rootScope
    * @requires $window
    *
    * @description
    *  Manage enviroment that return an object api
    *
    * @returns {object} api abject to use.
    */
    function MainEnv($rootScope, $window, dcyUtilsFactory) {

      var envVars = $rootScope.applicationSession = window.dcyAngular.SESSION_DATA;

      /**
       * @ngdoc method
       * @name update
       * @methodOf dcyApp.dcyEnvProvider.MainEnv
       *
       * @description
       * store new pairs to factory
       *
       * @param {string} key the key under which the data is stored.
       * @param {string} value the value to store alongside the key. If it is undefined, the key
       *    will not be stored.
       * @returns {*} the value stored.
       */
      this.update = function(newData) {
        if (dcyUtilsFactory.isValidObject(newData)) {
          if (angular.equals(newData.contentUID, envVars.contentUID) /*&& !angular.equals(envVars.data, newData.data)*/) {
            envVars.data = newData.data;
            return true;
          }
        }
        return false;
      };

      /**
       * @ngdoc method
       * @name getVariable
       * @methodOf dcyApp.dcyEnvProvider.MainEnv
       *
       * @description
       * get the stored value
       *
       * @param {string} key the key under which the data is stored.
       * @returns {*} the value of stored item.
       */
      function getVariable(srcKey) {

        var item;

        angular.forEach(envVars.data, function(value, key) {
          var getParam = value[srcKey];

          if (angular.isUndefined(item)) {
            if (angular.isDefined(getParam)) {
              item = getParam;
            } else if (angular.equals(key, srcKey)) {
              item = value;
            }
          }
        });

        return item;
      }

      function searchItemInObject(data, srcKey){
    	  var result;

    	  var iterateObject = function(value, srcKey){
    		  angular.forEach(value, function(value, key) {
    			  if(angular.equals(key, srcKey) && angular.isUndefined(result)){
    				  result = value;
    			  }

    			  if(angular.isObject(value)){
    				  iterateObject(value, srcKey);
    			  }
    		  });
    	  };

    	  iterateObject(data, srcKey);

		  return result;
	  }

      /**
       * @ngdoc method
       * @name putVariable
       * @methodOf dcyApp.dcyEnvProvider.MainEnv
       *
       * @description
       * store new pairs to factory
       *
       * @param {string} key the key under which the data is stored.
       * @param {string} value the value to store alongside the key. If it is undefined, the key
       *    will not be stored.
       * @returns {*} the value stored.
       */
      this.putVariable = function(srcKey, srcItem, newValue, rootNode) {

    	  if (angular.isUndefined(newValue) && (!dcyUtilsFactory.isValidString(srcKey) || !dcyUtilsFactory.isValidString(srcItem))) {
    		  return false;
    	  }

    	  var callback = function(data, srcKey, srcItem){
    		  var item = searchItemInObject(data, srcKey);
    		  if(angular.isDefined(item)){
    			  item[srcItem] = newValue;
    			  if (!$rootScope.$$phase) {
    				  $rootScope.$apply();
    			  }
    			  return true;
    		  }
    		  return undefined;
    	  },
    	  data = angular.isDefined(rootNode) && angular.isObject(envVars.data[rootNode]) ? envVars.data[rootNode] : envVars.data;

    	  return callback(data, srcKey, srcItem);
      };



      /**
       * @ngdoc method
       * @name getAll
       * @methodOf dcyApp.dcyEnvProvider.MainEnv
       *
       * @description
       * Get ALL stored value
       *
       * @returns {object} envVars the value of stored item.
       */
      this.getAll = function() {
        return envVars;
      };

      /**
       * @ngdoc method
       * @name getItem
       * @methodOf dcyApp.dcyEnvProvider.MainEnv
       *
       * @description
       * get context path of application
       *
       * @returns {string} context path.dcyEnv.getItem('sidebarIsVisible', 'sidebar','ui');
       */
      this.getItem = function(srcItem, srcKey, rootNode) {

    	  if (!dcyUtilsFactory.isValidString(srcItem)) {
    		  return false;
    	  }

    	  var callback = function(data, fieldToSearch){
	    		  var takenField = searchItemInObject(data, fieldToSearch);
	    		  if(angular.isDefined(takenField)){
	    			  return dcyUtilsFactory.isValidObject(takenField) && angular.isDefined(takenField[srcItem]) ? takenField[srcItem] : takenField;
	    		  }
	    		  return undefined;
	    	  },
	    	  data = angular.isDefined(rootNode) && angular.isObject(envVars.data[rootNode]) ? envVars.data[rootNode] : envVars.data;

    	  var fieldToSearch = dcyUtilsFactory.isValidString(srcKey) ? srcKey : srcItem;

    	  return callback(data, fieldToSearch, srcKey);
      };

      /**
       * @ngdoc method
       * @name getContextPath
       * @methodOf dcyApp.dcyEnvProvider.MainEnv
       *
       * @description
       * get context path of application
       *
       * @returns {string} context path.
       */
      this.getContextPath = function() {
        return getVariable('contextPath');
      };

      /**
       * @ngdoc method
       * @name isInDevelopment
       * @methodOf dcyApp.dcyEnvProvider.MainEnv
       *
       * @description
       * get status information (yes: development; false:
       *  production)
       *
       * @returns {boolean} status.
       */
      this.isInDevelopment = function() {
        var result = (getVariable('isInDevelopment') === 'true');
        return result;
      };

      /**
       * @ngdoc method
       * @name isInTesting
       * @methodOf dcyApp.dcyEnvProvider.MainEnv
       *
       * @description
       * get status information (yes: testing; false:
       *  development or production)
       *
       * @returns {boolean} status.
       */
      this.isInTesting = function() {
        var result = (getVariable('isInTesting') === 'true');
        return result;
      };

      /**
       * @ngdoc method
       * @name isInDevelopmentOrTesting
       * @methodOf dcyApp.dcyEnvProvider.MainEnv
       *
       * @description
       * get status information (yes: development/testing; false:
       *  production)
       *
       * @returns {boolean} status.
       */
      this.isInDevelopmentOrTesting = function() {
        var result = (getVariable('isInDevelopmentOrTesting') === 'true');
        return result;
      };


      /**
       * @ngdoc method
       * @name getCurrentUserID
       * @methodOf dcyApp.dcyEnvProvider.MainEnv
       *
       * @description
       * get current user identifier
       *
       * @returns {integer} userId.
       */
      this.getCurrentUserID = function() {
        return getVariable('currentUserID');
      };

      /**
       * @ngdoc method
       * @name getUserLanguage
       * @methodOf dcyApp.dcyEnvProvider.MainEnv
       *
       * @description
       * get current user language
       *
       * @returns {string} language id.
       */
      this.getUserLanguage = function() {
        return getVariable('prefererLanguage');
      };


      this.getShLibTokenizedPath = function() {
    	  return ENV_VARS.getShLibToken();
      };

      this.getTemplate = function($element) {
    	  var 	$dcycontent = $element.closest('dcycontent');

    	  if(!$dcycontent.length){
    		  return false;
    	  }

    	  var $scope = $dcycontent.isolateScope();

    	  var Template = $scope.Template;

    	  if(!window.$.isEmptyObject(Template)){
    		 return Template;
    	  }

    	  return undefined;

      };

    }

    this.$inject = ['$rootScope', '$window', 'dcyUtilsFactory', '$log'];

    this.$get = ['$rootScope', '$window', 'dcyUtilsFactory', function($rootScope, $window, dcyUtilsFactory) {
        return new MainEnv($rootScope, $window, dcyUtilsFactory);
      }
    ];

  }

  angular
    .module('dcyApp.providers')
    .provider('dcyEnvProvider', EnvProvider);
}());
