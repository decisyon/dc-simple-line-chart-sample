(function() {
	'use strict';

	function DcyService(dcyRenderingEnviromentFactory, dcyUtilsFactory, $translate, $q, dcyTranslateProvider) {

		var getResourcePath = function(logicalName) {
			return dcyRenderingEnviromentFactory.getResourcePathByLogicalName(logicalName);
		};

		var getLibraryPath = function(logicalName) {
			return dcyUtilsFactory.getLibraryPath(logicalName);
		};

		var getTranslation = function(labelId, interpolateParams, language){
			return $translate.instant("_" + labelId, interpolateParams, null, language);
		};

		var addTranslations = function(jsonLabels, language) {
			var json = {};
			language = dcyUtilsFactory.isValidString(language) ? language : dcyTranslateProvider.translateProvider.preferredLanguage();

			angular.forEach(jsonLabels, function(value, key) {
				json["_" + key] = value;
			});
			dcyTranslateProvider.translateProvider.translations(language, json);
		};

		return {
			getResourcePath			: getResourcePath,
			getLibraryPath			: getLibraryPath,
			getTranslation 			: getTranslation,
			addTranslations			: addTranslations
		};

	}

	DcyService.$inject = ['dcyRenderingEnviromentFactory', 'dcyUtilsFactory', '$translate', '$q', 'dcyTranslateProvider'];
	angular.module('dcyApp.factories').factory('dcyService', DcyService);
}());
