(function() {
    'use strict';

    function DCSimpleLineChartController($scope, $timeout, $q) {
        $scope.DECISYON.target.registerDataConnector(function(requestor) {

            var defered = $q.defer();

            $timeout(function() {

                defered.resolve([
                    [1397102460000, 1.99],
                    [1397139660000, 1.92],
                    [1397177400000, 1.97],
                    [1397228040000, 1.12],
                    [1397248260000, 1.09],
                    [1397291280000, 1],
                    [1397318100000, 1.99],
                    [1397342100000, 1.75],
                    [1397390820000, 1.11],
                    [1397408100000, 1.93],
                    [1397458800000, 1.84],
                    [1397522940000, 1.99]
                ]);

            }, 3000);

            return {
                data: defered.promise

            };

        });
    }

    DCSimpleLineChartController.$inject = ['$scope', '$timeout', '$q'];
    DECISYON.ng.register.controller('dcSimpleLineChartCtrl', DCSimpleLineChartController);

}());